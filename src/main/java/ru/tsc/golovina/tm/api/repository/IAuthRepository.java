package ru.tsc.golovina.tm.api.repository;

public interface IAuthRepository {

    String getCurrentUserId();

    void setCurrentUserId(String currentUserId);

}
