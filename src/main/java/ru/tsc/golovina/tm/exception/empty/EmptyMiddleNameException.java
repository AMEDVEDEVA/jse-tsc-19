package ru.tsc.golovina.tm.exception.empty;

import ru.tsc.golovina.tm.exception.AbstractException;

public class EmptyMiddleNameException extends AbstractException {

    public EmptyMiddleNameException() {
        super("Error. Middle name is empty.");
    }

}
