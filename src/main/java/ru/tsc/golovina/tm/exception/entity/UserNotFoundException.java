package ru.tsc.golovina.tm.exception.entity;

import ru.tsc.golovina.tm.exception.AbstractException;

public class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("Error. User not found.");
    }

}
