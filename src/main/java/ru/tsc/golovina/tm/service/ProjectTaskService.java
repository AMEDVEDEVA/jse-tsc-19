package ru.tsc.golovina.tm.service;

import ru.tsc.golovina.tm.api.repository.IProjectRepository;
import ru.tsc.golovina.tm.api.repository.ITaskRepository;
import ru.tsc.golovina.tm.api.service.IProjectTaskService;
import ru.tsc.golovina.tm.model.Project;
import ru.tsc.golovina.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    public ProjectTaskService(ITaskRepository taskRepository, IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Task> findTaskByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (!projectRepository.existsById(projectId)) return null;
        return taskRepository.findAllTaskByProjectId(projectId);
    }

    @Override
    public Task bindTaskById(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        if (!projectRepository.existsById(projectId) || !taskRepository.existsById(taskId)) return null;
        return taskRepository.bindTaskToProjectById(projectId, taskId);
    }

    @Override
    public Task unbindTaskById(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        if (!projectRepository.existsById(projectId) || !taskRepository.existsById(taskId)) return null;
        return taskRepository.unbindTaskById(taskId);
    }

    @Override
    public Project removeById(final String projectId) {
        taskRepository.removeAllTaskByProjectId(projectId);
        return projectRepository.removeById(projectId);
    }

    @Override
    public Project removeByIndex(final Integer index) {
        Project project = projectRepository.findByIndex(index);
        String projectId = project.getId();
        taskRepository.removeAllTaskByProjectId(projectId);
        return projectRepository.removeById(projectId);
    }

    @Override
    public Project removeByName(final String name) {
        Project project = projectRepository.findByName(name);
        String projectId = project.getId();
        taskRepository.removeAllTaskByProjectId(projectId);
        return projectRepository.removeById(projectId);
    }

}
