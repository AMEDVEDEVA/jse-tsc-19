package ru.tsc.golovina.tm;

import ru.tsc.golovina.tm.component.Bootstrap;

public class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}